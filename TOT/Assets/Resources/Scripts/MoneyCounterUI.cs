﻿using UnityEngine;
using UnityEngine.UI;

public class MoneyCounterUI : MonoBehaviour {

    private static Text moneyText;
    public static int coinCount = 5;
    void Awake()
    {
        moneyText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnGUI()
    {
        string coinTextCount = "Total Coins: " + coinCount;
        moneyText.text = "Coins Left : " + GameMaster.Money.ToString();
    }
}
