﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCounter : MonoBehaviour {

    public static int coinCount = 5;

    private void OnGUI()
    {
        string coinTextCount = "Total Coins: " + coinCount;
        GUI.Box(new Rect(Screen.width - 150, 20, 130, 20), coinTextCount);
    }

}
