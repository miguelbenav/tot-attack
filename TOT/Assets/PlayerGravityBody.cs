﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGravityBody : MonoBehaviour {

    public PlanetScript attractorPlanet;
    private Transform playerTransform;
    private bool useGravity;

    void Start()
    {
       
        /* GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;*/

        playerTransform = transform;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            useGravity = false;
        }
    }

    void FixedUpdate()
    {
        if (attractorPlanet)
        {
            attractorPlanet.Attract(playerTransform);
        }
    }
}
